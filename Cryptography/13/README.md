# 13 - picoCTF



## Descrizione

![img1](img1.jpg)

# Soluzione

In questo caso la sfida è molto semplice, dato che ci viene espressamente detto che si tratta di un cifrario ROT13. 
Quindi sarà sufficiente incollare la stringa data in un programma che lo decodifichi, come per esempio [CyberChef](https://gchq.github.io/CyberChef/).

![img2](img2.jpg)

Ed ecco trovata la nostra flag!

# Approfondimento

Il ROT13 è un semplice cifrario monoalfabetico, è una variante del cifrario Cesare, ma con chiave 13. Significa che ogni lettera è sostituita con quella posta 13 posizioni più avanti nell'alfabeto.

![](img3.jpg)

Di seguito il link per il video della soluzione:
[https://www.youtube.com/watch?v=jRtR5JNq2B8](https://www.youtube.com/watch?v=jRtR5JNq2B8)