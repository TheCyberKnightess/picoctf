# Wireshark twoo twooo two twoo - picoCTF



## Descrizione

![img1](img1.jpg)

# Soluzione

Ci troviamo davanti un file pcapng, un file in cui vengono registrate le tracce dei pacchetti di rete. Lo apriamo con Wireshark:

![img2](img2.jpg)

Ci sono davvero tanti pacchetti, troppi per guardarli uno per uno, usiamo il comando <code>tshark -qz io,psh -r shark2.pcapng</code> per vedere quanti pacchetti ci sono per ogni protocollo.

![img3](img3.jpg)

Un alto traffico DNS potrebbe essere sospetto, andiamo a indagare su quei pacchetti. 

![img4](img4.jpg)

Vediamo che per la maggior parte proviene dallo stesso IP, ma andiamo a verificare che non ce ne siano altri.

![img5](img5.jpg)

Infatti ne vediamo un altro il <code>18.217.1.57</code>. Torniamo su Wireshark e andiamo a vedere le richieste verso quell'indirizzo.

![img6](img6.jpg)

Notiamo che il dominio è sempre lo stesso: reddshrimpandherring.com, ma il sottominio cambia, in particolare ne vediamo uno significativo: <code>fQ==</code>, il che ci porta a pensare che ci sia nascosto un base64.
<br>
Con il seguente comando andiamo ad estrarre tutti i sottodomini da questi pacchetti: <code> tshark -nr shark2.pcapng -Y 'dns && ip-src == 192.168.38.104 && frame contains "local" && ip.dst == 18.217.1.57 | awk '{ print $12 }' | awk -F. '{ print $1 }' | tr -d "\n"</code>
<br> 
E andiamo a decodificarlo.

![img7](img7.jpg)

Ecco trovata la nostra flag!

# Approfondimento

<code>tshark -qz io,psh -r shark2.pcapng</code>
- **-qz io,psh** sono le opzioni di output che specificano i conteggi dei pacchetti in cui è stata stabilita una connessione con una "push" finale, ovvero il flag PSH impostato nell'ultimo pacchetto di una connessione.
- **-r shark2.pcapng** specifica il file di cattura del traffico di rete che si desidera analizzare.

<code> tshark -nr shark2.pcapng -Y 'dns && ip-src == 192.168.38.104 && frame contains "local" && ip.dst == 18.217.1.57 | awk '{ print $12 }' | awk -F. '{ print $1 }' | tr -d "\n"</code>

Filtra i pacchetti di rete in base ai seguenti criteri:

- solo i pacchetti DNS (dns)
- solo i pacchetti con l'indirizzo IP di origine 192.168.38.104 (ip-src == 192.168.38.104)
- solo i pacchetti che contengono la stringa "local" (frame contains "local")
- solo i pacchetti che hanno l'indirizzo IP di destinazione 18.217.1.57 (ip.dst == 18.217.1.57)


(Questi criteri sono specificati con l'opzione -Y seguita da una stringa di filtro.)

- Usa il comando **awk '{ print $12 }'** per stampare solo la colonna 12 del risultato del filtro, ovvero la sezione "risposta" del pacchetto DNS che contiene l'indirizzo IP cercato.
- Usa il comando **awk -F. '{ print $1 }'** per stampare solo la prima sezione dell'indirizzo IP trovato, separando l'indirizzo IP in base al carattere ".".
- Usa il comando **tr -d "\n"** per eliminare eventuali caratteri di nuova linea (\n) dal risultato.



Di seguito il link per il video della soluzione:
[https://www.youtube.com/watch?v=RgAT1XlFelk](https://www.youtube.com/watch?v=RgAT1XlFelk)