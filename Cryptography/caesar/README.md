# caesar - picoCTF



## Descrizione

![img1](img1.jpg)

# Soluzione

All'interno dell messaggio vediamo una flag crittografata, e come suggerisce il titolo della challenge è molto probabile che sia stato usato il cifrario di Cesare. 

![img2](img2.jpg)

Per decriptarlo possiamo usare un tool online, come questo per esempio [https://www.dcode.fr/caesar-cipher](https://www.dcode.fr/caesar-cipher).

![img3](img3.jpg)

Ma tra i tanti risultati come facciamo a sapere qual è quella giusta?
Proviamo quella che sembra avere un collegamento con Cesare: il famoso passaggio del Rubicone.

![img4](img4.jpg)

Ed ecco trovata la nostra flag!

# Approfondimento

Il cifrario di Cesare è un cifrario a sostituzione monoalfabetica, in cui ogni lettera del testo in chiaro è sostituita, nel testo cifrato, dalla lettera che si trova un certo numero di posizioni dopo nell'alfabeto.

![img5](img5.png)



Di seguito il link per il video della soluzione:
[https://www.youtube.com/watch?v=KK4rr6qbd40](https://www.youtube.com/watch?v=KK4rr6qbd40)