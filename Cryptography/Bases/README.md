# Bases - picoCTF



## Descrizione

![img1](img1.jpg)

# Soluzione

 Dato il titolo della challenge possiamo ipotizzare che si tratti di una codifica di caratteri di un tipo di base. Una della più usate è quella in base64 quindi proveremo quella.

 Per decodificarlo possiamo usare un tool online, come questo per esempio [https://www.dcode.fr/caesar-cipher](https://www.dcode.fr/caesar-cipher).


![img2](img2.jpg)

Ha funzionato! Ecco la nostra flag!


![img3](img3.jpg)

# Approfondimento

Base64 è un sistema di codifica che consente la traduzione di dati binari in stringhe di testo ASCII, rappresentando i dati sulla base di 64 caratteri ASCII diversi. 

L'algoritmo causa un aumento delle dimensioni dei dati del 33%, poiché ogni gruppo di 3 byte viene convertito in 4 caratteri.

Se la lunghezza del messaggio originale non è un multiplo di 3 byte il numero dei bit che costituiscono il risultato non sarà un multiplo di 6. Verranno quindi inseriti bit nulli (0) alla fine (4 o 2), e nel valore codificato vengono aggiunti da 0 a 2 simboli '=' (padding character) sufficienti a raggiungere un multiplo di 4 simboli. Ciascun padding character indica pertanto l'aggiunta di una coppia di bit nulli. Il padding non è comunque indispensabile per la decodifica e alcune implementazioni non lo utilizzano. Il padding è indispensabile solo qualora si vogliano concatenare messaggi codificati.


Di seguito il link per il video della soluzione:
[https://www.youtube.com/watch?v=tQw63S7eJU0](https://www.youtube.com/watch?v=tQw63S7eJU0)